# -*- coding: utf-8 -*-
"""
Created on Thu Nov  3 15:46:17 2016

@author: e5108121
"""
import runDMonth as rm
from pandas import ExcelWriter
# --[(STEP 1): Set the station number]--
station = '76015'
# --[(STEP 2): Set the number of X vaiables]--
xdim = 6
months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July',
          'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
#months = ['Jan','Feb']
params = {
          # --[(STEP 3): Set the number of filters between 3-20]--
          'nb_filter': 16,
          # --[(STEP 4): Set the loss function]--
          'f_loss': 'mean_squared_error',
          # {Available options: 'mean_squared_error', 'mean_absolute_error',
          # 'mean_absolute_percentage_error', 'mean_squared_logarithmic_error'
          # 'squared_hinge', 'hinge','binary_crossentropy','mean_squared_error'
          # 'kullback_leibler_divergence', 'poisson'}
          # -----------
          # --[(STEP 5): Set the percentage of validation]--
          'vali_pct': 10/100,
          # --[(STEP 6): Set the number of epoches]--
          'n_epoch': 1000,
          # --[(STEP 7): Set the training batch size]--
          'batch_size': 4,
          # --[(STEP 8): Set the optimizer]--
          'optimizer': 'adam',
          # {Available options: 'adam', 'sgd', 'rmsprop','adagrad', 'adadelta'
          # 'adamax', 'nadam'}
          # --[(STEP 9): Set the optimizer]--
          'activation': 'relu'
          # {Available options: 'softmax', 'softplus', 'relu','softsign',
          # 'linear', 'sigmoid','hard_sigmoid', 'tanh'}
          }
writer = ExcelWriter('stations/'+station+'/Summary.xlsx')
for month in months:
    print('Running %s ...\n' % (month))
    table = rm.runmonth(params, station, month, xdim, writer)
    table.to_excel(writer, sheet_name=month, index=False)
writer.save()
