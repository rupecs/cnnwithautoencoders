# -*- coding: utf-8 -*-
"""
Created on Tue Nov 15 12:07:10 2016

@author: e5108121
"""
from __future__ import print_function, division
import matplotlib.pyplot as plt
import pandas
import math
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Convolution1D
from keras.layers import MaxPooling1D
from keras.layers import Flatten
from sklearn.metrics import mean_squared_error
from pandas import DataFrame
import numpy as np

def runmonth(params, station, month, xdim, writer):
    np.random.seed(7)
    califile = 'stations/'+station+'/'+month+'_Calib_'+station+'.csv'
    valifile = 'stations/'+station+'/'+month+'_Vali_'+station+'.csv'
    dataframe_cali = pandas.read_csv(califile, delimiter=";", engine='python')
    dataset_cali = dataframe_cali.values
    trainX, trainY = dataset_cali[:, 0:xdim], dataset_cali[:, xdim]
    dataframe_vali = pandas.read_csv(valifile, delimiter=";", engine='python')
    dataset_vali = dataframe_vali.values
    testX, testY = dataset_vali[:, 0:xdim], dataset_vali[:, xdim]
    [trainPredict, testPredict, trainScore, testScore] = runmodel(
        trainX, trainY, testX, testY, params, xdim)
    f, axarr = plt.subplots(2, sharex=False)
    axarr[0].plot(trainY, 'b')
    axarr[0].plot(trainPredict, 'r')
    axarr[0].set_title(month)
    axarr[1].plot(testY, 'b')
    axarr[1].plot(testPredict, 'r')
    prediction = np.concatenate((trainPredict, testPredict), axis=0)
    prediction = np.reshape(prediction, prediction.shape[0])
    observed = np.concatenate((trainY, testY), axis=0)
    observed = np.reshape(observed, observed.shape[0])
    table = DataFrame({'Prediction': np.transpose(prediction),
                       'Observed': np.transpose(observed)})
    rmsetable = DataFrame({'RMSE training': [trainScore],
                           'RMSE testing': [testScore]})
    table = table.join(rmsetable)
    return table


def runmodel(trainX, trainY, testX, testY, params, xdim):
    # reshape input to be [samples, time steps, features]
    f_act = params['activation']
    model = Sequential()
    model.add(Dense(4, input_dim=6,init='uniform'))
    model.add(Dense(3, activation=f_act))
    model.add(Dense(2, activation=f_act))
    model.add(Dense(3, activation=f_act))
    model.add(Dense(4, activation=f_act))
    model.add(Dense(6, activation=None))
    model.compile(loss=params['f_loss'], optimizer=params['optimizer'])
    print('\n\nAutoencoder with input size {}, output size {}'
          .format(model.input_shape, model.output_shape))
    model.summary()
    model.fit(
              trainX, trainX,
              nb_epoch=params['n_epoch'],
              batch_size=params['batch_size'],
              validation_split=params['vali_pct'],
              verbose=2
              )
    intTrainPredict = model.predict(trainX)
    intTestPredict = model.predict(testX)
    trainX2 = np.reshape(intTrainPredict,(intTrainPredict.shape[0],
                                          intTrainPredict.shape[1], 1))
    trainY2 = np.reshape(trainY, (trainY.shape[0], 1))
    testX2 = np.reshape(intTestPredict, (intTestPredict.shape[0],
                                         intTestPredict.shape[1], 1))
    testY2 = np.reshape(testY, (testY.shape[0], 1))
    filter_length = 3
    nb_filter = params['nb_filter']
    model2 = Sequential((
            Convolution1D(nb_filter=nb_filter, filter_length=filter_length,
                          activation=f_act, input_shape=(xdim, 1)),
            # MaxPooling1D(),     
            Convolution1D(nb_filter=nb_filter, filter_length=filter_length,
                          activation=f_act),
            # Convolution1D(nb_filter=nb_filter, filter_length=filter_length,
            #              activation='relu'),
            # MaxPooling1D(),
            Flatten(),
            Dense(1, activation='linear'),
        ))
    model2.compile(loss=params['f_loss'], optimizer=params['optimizer'])
    print('\n\nCNN with input size {}, output size {}, {} conv filters of length {}'
          .format(model2.input_shape, model2.output_shape, nb_filter, filter_length))
    model2.summary()
    model2.fit(trainX2, trainY2,
              nb_epoch=params['n_epoch'],
              batch_size=params['batch_size'],
              #validation_data=(testX2, testY2)
              validation_split=params['vali_pct'],
              verbose=2
              )
    train = model2.predict(trainX2)
    trainPredict = train.squeeze()
    trainPredict[trainPredict < 0] = 0
    pred = model2.predict(testX2)
    testPredict = pred.squeeze()
    testPredict[testPredict < 0] = 0
    print('\n\nactual', 'predicted', sep='\t')
    for actual, predicted in zip(trainY2, train.squeeze()):
        print(actual.squeeze(), predicted, sep='\t')
    for actual, predicted in zip(testY2, pred.squeeze()):
        print(actual.squeeze(), predicted, sep='\t')
    trainScore = math.sqrt(mean_squared_error(trainY, trainPredict))
    print('Train Score: %.2f RMSE' % (trainScore))
    testScore = math.sqrt(mean_squared_error(testY, testPredict))
    print('Test Score: %.2f RMSE \n' % (testScore))
    return [trainPredict, testPredict, trainScore, testScore]
